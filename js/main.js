let outImage = "showimage";
function preview(obj) {
    if (FileReader) {
        let reader = new FileReader();
        reader.readAsDataURL(obj.files[0]);
        reader.onload = function (e) {
            let image = new Image();
            image.src = e.target.result;
            image.onload = function () {
                document.getElementById(outImage).src = image.src;
            };
        }
    }
    else {
    }
}

document.getElementById('postData').onclick = function () {
    let title = document.querySelector('#title').value;
    let url = document.querySelector('#url').value;
    let text = document.querySelector('#text').value;
    let showimage = document.querySelector('#showimage').src;
    let postDatta = document.querySelector('#postDatta');
    let showMaxBlock = 10;
    if (title < 1) {
        alert('Додайте заголовок');
    } else if (url < 1) {
        alert('Додайте посилання')
    } else if (text < 1) {
        alert('Додайте текст')
    } else if (showimage.length < 1) {
        alert('Додайте зображення')
    }
    else if (title.match(/[^а-яА-ЯїЇєЄіІёЁ ]/) || text.match(/[^а-яА-ЯїЇєЄіІёЁ ]/)) {
        alert('Заголовок і текст ведіть кирилицею')
    }
    else {
        createBlock(title, url, text, showimage, postDatta);
        clearinput();
        let arrayblock = Array.from(document.querySelectorAll('.pos .style_blocks'));
        console.log(arrayblock)

        arrayblock.slice(showMaxBlock).forEach(function (hideBlocks) {
            hideBlocks.style.display = 'none'

        })
        if (arrayblock.length > showMaxBlock) {


            document.querySelector('#nextBlock').style.display = 'block';
            document.getElementById('nextBlock').onclick = function () {
                let addNextBlock = 11;
                let fullArrayBlock = showMaxBlock + addNextBlock;
                let moreBlock = arrayblock.slice(showMaxBlock, fullArrayBlock)
                moreBlock.forEach(
                    function (showBlocks) {
                        showBlocks.style.display = 'block'
                    }
                );
                if (fullArrayBlock > arrayblock.length) {
                    document.querySelector('#nextBlock').style.display = 'none'
                }

            }

        }
    }
};
function createBlock(title, url, text, showimage, postDatta) {
    let wrapperBlocks = document.createElement('div');
    wrapperBlocks.classList.add('style_blocks')
    wrapperBlocks.setAttribute('id', 'rarara');
    let a = document.createElement('a');
    a.setAttribute('href', url);
    let div = document.createElement('div');
    let image = document.createElement('img');
    image.classList.add('size_img');
    image.setAttribute('src', showimage);
    let h1 = document.createElement('h1');
    let p = document.createElement('p');
    postDatta.append(wrapperBlocks)
    wrapperBlocks.append(a);
    a.append(div);
    div.append(image);
    a.append(h1, p);
    h1.textContent = title;
    p.textContent = text;
}
function clearinput() {
    document.querySelector('#title').value = "";
    document.querySelector('#url').value = "";
    document.querySelector('#text').value = "";
    document.querySelector('#showimage').removeAttribute("src");
    document.getElementById('addimg').value = '';
}





// function load(progress) {

//     progress.innerHTML = `
//         <progress class="hero-list progress is-medium is-info" max="100"></progress>
//     `;

// }